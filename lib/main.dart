import 'package:flutter/material.dart';
import 'package:orders/pages/orders.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Orders',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Orders(),
    );
  }
}
