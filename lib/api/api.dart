import 'package:orders/api/dio.dart';

class Api extends DioApi {
  Future getProducts() async {
    return dio.get("/products");
  }

  Future getSuppliers() async {
    return dio.get("/suppliers");
  }

  Future getOrders() async {
    return dio.get("/orders");
  }
}

var api = Api();
