import 'package:dio/dio.dart';
import 'package:orders/shared/strings.dart';

class DioApi {
  Dio dio;

  DioApi() {
    dio = Dio();
    dio.options.baseUrl = apiUrl;
    dio.options.connectTimeout = 5000;
    dio.options.receiveTimeout = 5000;
    dio.interceptors.add(InterceptorsWrapper(onRequest: _requestIntercept));
    dio.interceptors.add(InterceptorsWrapper(onResponse: _responseIntercept));
    dio.interceptors.add(InterceptorsWrapper(onError: _errorIntercept));
  }

  _requestIntercept(RequestOptions options) async {
    print("${options.method}: ${options.baseUrl}${options.path}");
    print("BODY: ${options.data}");
    return options;
  }

  _responseIntercept(Response response) async {
    return response;
  }

  _errorIntercept(DioError error) async {
    return error;
  }
}
