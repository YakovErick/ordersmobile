import 'package:flutter/material.dart';
import 'package:orders/models/supplier.dart';

class SupplierPage extends StatefulWidget {
  final Supplier supplier;

  @override
  _SupplierState createState() => _SupplierState(supplier);

  SupplierPage(this.supplier);
}

class _SupplierState extends State<SupplierPage> {
  Supplier _supplier;

  _SupplierState(this._supplier);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Supplier"),
        ),
        body: Container(
            child: Column(children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Card(
                  elevation: 0.5,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        padding: EdgeInsets.all(5.0),
                        height: 150,
                        child: Center(
                          child: Image.asset('assets/images/avatar.png'),
                        ),
                      ),
                      Card(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Icons.account_circle),
                                Text(_supplier.name,
                                    style: TextStyle(
                                        fontSize: 20.0, color: Colors.black))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.email),
                                Text(_supplier.email,
                                    style: TextStyle(fontSize: 15.0))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.my_location),
                                Text(_supplier.address,
                                    style: TextStyle(fontSize: 15.0))
                              ],
                            ),
                          ],
                        ),
                      ),
                      Text("Products"),
                      Card(
                        child: Column(
                          children: _supplier.products.map((_product) {
                            return ListTile(
                              title: Text(_product.name),
                            );
                          }).toList(),
                        ),
                      )
                    ],
                  ),
                )
              ])
        ])));
  }
}
