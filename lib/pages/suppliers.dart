import 'package:flutter/material.dart';
import 'package:orders/api/api.dart';
import 'package:orders/models/supplier.dart';
import 'package:orders/pages/supplier.dart';

class Suppliers extends StatefulWidget {
  @override
  _SuppliersState createState() => _SuppliersState();
}

class _SuppliersState extends State<Suppliers> {
  List<Supplier> suppliers = [];
  bool isLoading = false;

  @override
  void initState() {
    _fetchSuppliers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Suppliers"),
      ),
      body: Container(
        child: isLoading
            ? Center(child: Text('Loading...'))
            : ListView.builder(
                itemCount: suppliers.length,
                itemBuilder: (context, index) {
                  Supplier supplier = suppliers[index];
                  return GestureDetector(
                    child: Card(
                        child: ListTile(
                            leading: Text("${supplier.id}"),
                            title: Text("${supplier.name}"),
                            subtitle: Text("${supplier.email}",
                                style: TextStyle(fontSize: 15.0)))),
                    onTap: () async {
                      await Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SupplierPage(supplier)));
                    },
                  );
                }),
      ),
    );
  }

  _fetchSuppliers() async {
    setState(() {
      isLoading = true;
    });
    var response = await api.getSuppliers();
    var data = response.data['data'];
    List<Supplier> _suppliers = [];
    for (var supplierJson in data) {
      Supplier _supplier = Supplier.fromJson(supplierJson);
      _suppliers.add(_supplier);
    }
    setState(() {
      isLoading = false;
      suppliers.addAll(_suppliers);
    });
  }
}
