import 'package:flutter/material.dart';
import 'package:orders/models/product.dart';

class ProductPage extends StatefulWidget {
  final Product product;

  @override
  _ProductState createState() => _ProductState(product);

  ProductPage(this.product);
}

class _ProductState extends State<ProductPage> {
  Product _product;

  _ProductState(this._product);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Product"),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text('Product Name: '),
                      Text(
                        _product.name,
                        style: TextStyle(fontSize: 20.0, color: Colors.black),
                      ),
                    ],
                  ),
                  Divider(),
                  Row(
                    children: <Widget>[
                      Text('Supplier: '),
                      Text(
                        _product.supplier,
                        style: TextStyle(fontSize: 20.0, color: Colors.black),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(),
              Text(_product.description)
            ],
          ),
        ),
      ),
    );
  }
}
