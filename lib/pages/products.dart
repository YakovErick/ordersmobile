import 'package:flutter/material.dart';
import 'package:orders/api/api.dart';
import 'package:orders/models/product.dart';
import 'package:orders/pages/product.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  List<Product> products = [];
  bool isLoading = false;

  @override
  void initState() {
    _fetchProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Products"),
      ),
      body: Container(
        child: isLoading
            ? Center(child: Text('Loading...'))
            : ListView.builder(
                itemCount: products.length,
                itemBuilder: (context, index) {
                  Product product = products[index];
                  return GestureDetector(
                    child: Card(
                        child: ListTile(
                            leading: Text("${product.id}"),
                            title: Text("${product.name}"),
                            subtitle: Text("${product.description}",
                                style: TextStyle(fontSize: 15.0)))),
                    onTap: () async {
                      await Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProductPage(product)));
                    },
                  );
                }),
      ),
    );
  }

  _fetchProducts() async {
    setState(() {
      isLoading = true;
    });
    var response = await api.getProducts();
    var data = response.data['data'];
    List<Product> _products = [];
    for (var productJson in data) {
      Product _product = Product.fromJson(productJson);
      _products.add(_product);
    }
    setState(() {
      isLoading = false;
      products.addAll(_products);
    });
  }
}
