import 'package:flutter/material.dart';
import 'package:orders/api/api.dart';
import 'package:orders/models/order.dart';
import 'package:orders/pages/products.dart';
import 'package:orders/pages/suppliers.dart';
import 'package:orders/shared/strings.dart';

class Orders extends StatefulWidget {
  Orders({Key key}) : super(key: key);

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  List<Order> orders = [];
  bool isLoading = false;
  double totalOrders = 0.0;
  double totalOrdersValue = 0.0;

  @override
  void initState() {
    _fetchOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
      ),
      drawer: _buildDrawer(context),
      body: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Text("Total Orders"),
                      Text('${totalOrders}')
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Text("Total Value"),
                      Text('${totalOrdersValue}')
                    ],
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: orders.length,
              itemBuilder: (context, index) {
                Order order = orders[index];
                return Card(
                  child: ListTile(
                    title: Text(order.branch),
                  ),
                );
              },
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Add Order',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Container(
                  child: UserAccountsDrawerHeader(
                      currentAccountPicture:
                          Image.asset('assets/images/avatar.png'),
                      accountName: Text("Roam Tech"),
                      accountEmail: Text("Orders")))),
          GestureDetector(
            child: ListTile(
              leading: Icon(Icons.directions_bus),
              title: Text('Suppliers'),
            ),
            onTap: () async {
              Navigator.pop(context);
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Suppliers()));
            },
          ),
          Divider(),
          GestureDetector(
            child: ListTile(
              leading: Icon(Icons.list),
              title: Text('Products'),
            ),
            onTap: () async {
              Navigator.pop(context);
              await Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Products()));
            },
          ),
        ],
      ),
    );
  }

  _fetchOrders() async {
    setState(() {
      isLoading = true;
    });
    var response = await api.getOrders();
    var data = response.data['data'];
    List<Order> _orders = [];
    for (var orderJson in data) {
      Order _order = Order.fromJson(orderJson);
      _orders.add(_order);
      totalOrders += 1;
      totalOrdersValue += _order.value;
    }
    setState(() {
      isLoading = false;
      orders.addAll(_orders);
    });
  }
}
