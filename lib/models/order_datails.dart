class OrderDetails {
  int id;
  int orderId;
  String product;
  int productId;
  String quantity;
  int number;
  int delivered;
  int balance;
  String date;
  int value;
  String valuePerItem;

  OrderDetails(
      {this.id,
      this.orderId,
      this.product,
      this.productId,
      this.quantity,
      this.number,
      this.delivered,
      this.balance,
      this.date,
      this.value,
      this.valuePerItem});

  OrderDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    product = json['product'];
    productId = json['product_id'];
    quantity = json['quantity'];
    number = json['number'];
    delivered = json['delivered'];
    balance = json['balance'];
    date = json['date'];
    value = json['value'];
    valuePerItem = json['value_per_item'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.orderId;
    data['product'] = this.product;
    data['product_id'] = this.productId;
    data['quantity'] = this.quantity;
    data['number'] = this.number;
    data['delivered'] = this.delivered;
    data['balance'] = this.balance;
    data['date'] = this.date;
    data['value'] = this.value;
    data['value_per_item'] = this.valuePerItem;
    return data;
  }
}
