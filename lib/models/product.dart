/*
* Product model class
* */
class Product {
  int id;
  String name;
  String description;
  String slug;
  String supplier;
  int supplierId;

  Product(
      {this.id,
      this.name,
      this.description,
      this.slug,
      this.supplier,
      this.supplierId});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    slug = json['slug'];
    supplier = json['supplier'];
    supplierId = json['supplier_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['slug'] = this.slug;
    data['supplier'] = this.supplier;
    data['supplier_id'] = this.supplierId;
    return data;
  }
}
