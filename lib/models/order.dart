import 'package:orders/models/order_datails.dart';

class Order {
  int id;
  List<OrderDetails> orderDetails;
  String date;
  int items;
  int value;
  String headquarter;
  String branch;

  Order(
      {this.id,
      this.orderDetails,
      this.date,
      this.items,
      this.value,
      this.headquarter,
      this.branch});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['order_details'] != null) {
      orderDetails = new List<OrderDetails>();
      json['order_details'].forEach((v) {
        orderDetails.add(new OrderDetails.fromJson(v));
      });
    }
    date = json['date'];
    items = json['items'];
    value = json['value'];
    headquarter = json['headquarter'];
    branch = json['branch'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.orderDetails != null) {
      data['order_details'] = this.orderDetails.map((v) => v.toJson()).toList();
    }
    data['date'] = this.date;
    data['items'] = this.items;
    data['value'] = this.value;
    data['headquarter'] = this.headquarter;
    data['branch'] = this.branch;
    return data;
  }
}
